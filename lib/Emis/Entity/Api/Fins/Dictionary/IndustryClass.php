<?php

namespace Emis\Entity\Api\Fins\Dictionary;

/**
 * 
 * @author dsales
 *
 */
class IndustryClass{
	
	/**
	 * @serializable true
	 * @var string
	 */
	private $induClass;
	
	/**
	 * @serializable true
	 * @var string
	 */
	private $className;
	
	/**
	 * @serializable true
	 * @var string
	 */
	private $countryCodes;
	

	/**
	 *
	 * @return the string
	 */
	public function getInduClass() {
		return $this->induClass;
	}
	
	/**
	 *
	 * @param string $induClass        	
	 */
	public function setInduClass( $induClass) {
		$this->induClass = $induClass;
		return $this;
	}
	
	/**
	 *
	 * @return the string
	 */
	public function getClassName() {
		return $this->className;
	}
	
	/**
	 *
	 * @param string $className        	
	 */
	public function setClassName( $className) {
		$this->className = $className;
		return $this;
	}
	
	/**
	 *
	 * @return the string
	 */
	public function getCountryCodes() {
		return $this->countryCodes;
	}
	
	/**
	 *
	 * @param string $countryCodes        	
	 */
	public function setCountryCodes( $countryCodes) {
		$this->countryCodes = $countryCodes;
		return $this;
	}
	
}
