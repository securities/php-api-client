<?php

namespace Emis\Entity\Api\Fins\Dictionary;

/**
 * 
 * @author dsales
 *
 */
class IndustryClassification{
	
	/**
	 * @serializable true
	 * @var string
	 */
	private $induClass;
	
	/**
	 * @serializable true
	 * @var string
	 */
	private $induCode;
	
	/**
	 * @serializable true
	 * @var string
	 */
	private $induName;

	/**
	 * @serializable true
	 * @var string
	 */
	private $induNameEng;

	/**
	 * @serializable true
	 * @var string
	 */
	private $parentInduCode;	
	
	/**
	 *
	 * @return the string
	 */
	public function getInduClass() {
		return $this->induClass;
	}
	
	/**
	 *
	 * @param string $induClass        	
	 */
	public function setInduClass( $induClass) {
		$this->induClass = $induClass;
		return $this;
	}
	
	/**
	 *
	 * @return the string
	 */
	public function getInduCode() {
		return $this->induCode;
	}
	
	/**
	 *
	 * @param string $induCode        	
	 */
	public function setInduCode( $induCode) {
		$this->induCode = $induCode;
		return $this;
	}
	
	/**
	 *
	 * @return the string
	 */
	public function getInduName() {
		return $this->induName;
	}
	
	/**
	 *
	 * @param string $induName        	
	 */
	public function setInduName( $induName) {
		$this->induName = $induName;
		return $this;
	}
	
	/**
	 *
	 * @return the string
	 */
	public function getInduNameEng() {
		return $this->induNameEng;
	}
	
	/**
	 *
	 * @param string $induNameEng        	
	 */
	public function setInduNameEng( $induNameEng) {
		$this->induNameEng = $induNameEng;
		return $this;
	}
	
	/**
	 *
	 * @return the string
	 */
	public function getParentInduCode() {
		return $this->parentInduCode;
	}
	
	/**
	 *
	 * @param string $parentInduCode        	
	 */
	public function setParentInduCode( $parentInduCode) {
		$this->parentInduCode = $parentInduCode;
		return $this;
	}
	
}
