<?php
/**
 * Created by PhpStorm.
 * User: dbenov
 * Date: 3/1/2019
 * Time: 6:20 PM
 */

namespace Emis\Entity\Api\Fins\Company;


class NumberRange
{
    /**
     * @var int
     */
    private $min = null;

    /**
     * @var int
     */
    private $max = null;

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param int $min
     * @return NumberRange
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     * @return NumberRange
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

}