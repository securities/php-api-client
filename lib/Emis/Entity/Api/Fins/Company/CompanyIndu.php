<?php

namespace Emis\Entity\Api\Fins\Company;

/**
 * Description of CompanyIndu
 *
 * @author dsales
 */
class CompanyIndu 
{
    /**
     * @var string
     */
    private $induClass = null;

    /**
     * @var string
     */
    private $induCode = null;

    /**
    * @return string
    */    
    public function getInduClass() 
    {
        return $this->induClass;
    }

    /**
    * @param string $induClass
    * @return CompanyIndu
    */
    public function setInduClass($induClass)
    {
        $this->induClass = $induClass;
        return $this;
    }

    /**
    * @return string
    */    
    public function getInduCode() 
    {
        return $this->induCode;
    }

    /**
    * @param string $induCode
    * @return CompanyIndu
    */
    public function setInduCode($induCode)
    {
        $this->induCode = $induCode;
        return $this;
    }
}
