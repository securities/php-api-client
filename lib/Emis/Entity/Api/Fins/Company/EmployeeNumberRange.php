<?php

namespace Emis\Entity\Api\Fins\Company;

/**
 * Description of EmployeeNumberRange
 *
 * @author jrutkowski
 */
class EmployeeNumberRange
{
    /**
     * @var int
     */
    private $min = null;

    /**
     * @var int
     */
    private $max = null;

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param int $min
     * @return EmployeeNumberRange
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $min
     * @return EmployeeNumberRange
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }
    
}
