<?php
namespace Emis\Entity\Api\Benchmark;

class Period
{
    /**
     * @var boolean
     */
    private $isUpdated;

    /**
     * @var int
     */
    private $companyCount;

    /**
     * @var int
     */
    private $fiscalYear;

    /**
     * @return boolean
     */
    public function isUpdated()
    {
        return $this->isUpdated;
    }

    /**
     * @param boolean $isUpdated
     * @return Period
     */
    public function setIsUpdated($isUpdated)
    {
        $this->isUpdated = $isUpdated;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompanyCount()
    {
        return $this->companyCount;
    }

    /**
     * @param int $companyCount
     * @return Period
     */
    public function setCompanyCount($companyCount)
    {
        $this->companyCount = $companyCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getFiscalYear()
    {
        return $this->fiscalYear;
    }

    /**
     * @param int $fiscalYear
     * @return Period
     */
    public function setFiscalYear($fiscalYear)
    {
        $this->fiscalYear = $fiscalYear;
        return $this;
    }
}