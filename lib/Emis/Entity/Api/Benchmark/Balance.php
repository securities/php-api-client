<?php

namespace Emis\Entity\Api\Benchmark;


class Balance
{

    /**
     * @var string $risk Risk scale of the company compared to the rest of the companies in the same industry based on the balance score.
     */
    private $risk;

    /**
     * @var float $balanceScore Total trend score on company balance-related indicators
     */
    private $balanceScore;

    /**
     * @var float $daysReceivablesScore Trend score on company days receivables indicator
     */
    private $daysReceivablesScore;

    /**
     * @var float $daysInventoryScore Trend score on company days inventory indicator
     */
    private $daysInventoryScore;

    /**
     * @var float $currentRatioScore Trend score on company current balance ratio indicator
     */
    private $currentRatioScore;

    /**
     * @var float $indebtnessScore Trend score on company indebtness (total assets vs total liabilities) indicator
     */
    private $indebtnessScore;

    /**
     * @var float $indebtednessSalesScore Trend score on company total assets vs sales indicator
     */
    private $indebtednessSalesScore;

    /**
     * @return string
     */
    public function getRisk()
    {
        return $this->risk;
    }

    /**
     * @param string $risk
     * @return Balance
     */
    public function setRisk($risk)
    {
        $this->risk = $risk;
        return $this;
    }

    /**
     * @return float
     */
    public function getBalanceScore()
    {
        return $this->balanceScore;
    }

    /**
     * @param float $balanceScore
     * @return Balance
     */
    public function setBalanceScore($balanceScore)
    {
        $this->balanceScore = $balanceScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getDaysReceivablesScore()
    {
        return $this->daysReceivablesScore;
    }

    /**
     * @param float $daysReceivablesScore
     * @return Balance
     */
    public function setDaysReceivablesScore($daysReceivablesScore)
    {
        $this->daysReceivablesScore = $daysReceivablesScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getDaysInventoryScore()
    {
        return $this->daysInventoryScore;
    }

    /**
     * @param float $daysInventoryScore
     * @return Balance
     */
    public function setDaysInventoryScore($daysInventoryScore)
    {
        $this->daysInventoryScore = $daysInventoryScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getCurrentRatioScore()
    {
        return $this->currentRatioScore;
    }

    /**
     * @param float $currentRatioScore
     * @return Balance
     */
    public function setCurrentRatioScore($currentRatioScore)
    {
        $this->currentRatioScore = $currentRatioScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getIndebtnessScore()
    {
        return $this->indebtnessScore;
    }

    /**
     * @param float $indebtnessScore
     * @return Balance
     */
    public function setIndebtnessScore($indebtnessScore)
    {
        $this->indebtnessScore = $indebtnessScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getIndebtednessSalesScore()
    {
        return $this->indebtednessSalesScore;
    }

    /**
     * @param float $indebtednessSalesScore
     * @return Balance
     */
    public function setIndebtednessSalesScore($indebtednessSalesScore)
    {
        $this->indebtednessSalesScore = $indebtednessSalesScore;
        return $this;
    }



}