<?php

namespace Emis\Entity\Api\Benchmark;


class CashFlow
{
    /**
     * @var string $risk Risk scale of the company compared to the rest of the companies in the same industry based on the cash flow score.
     */
    private $risk;

    /**
     * @var float $cashFlowScore Total trend score on company cash flow-related indicators
     */
    private $cashFlowScore;

    /**
     * @var float $cashSalesScore Trend score on company cash from sales indicator
     */
    private $cashSalesScore;

    /**
     * @var float $cashProductionScore Trend score on company cash for production indicator
     */
    private $cashProductionScore;

    /**
     * @var float $cashOperationsScore Trend score on company cash for operation indicator
     */
    private $cashOperationsScore;

    /**
     * @var float $operatingCashGenerationScore Trend score on company net operation cash generation (NOCG) indicator
     */
    private $operatingCashGenerationScore;

    /**
     * @var float $freeCashFlowScore Trend score on company free cash flow indicator
     */
    private $freeCashFlowScore;

    /**
     * @var float $cashFlowAfterFinExScore Trend score on company cash flow after financial expenses indicator
     */
    private $cashFlowAfterFinExScore;

    /**
     * @var float $cashFlowAfterFinResultScore Trend score on company cash flow after financial indicator
     */
    private $cashFlowAfterFinResultScore;

    /**
     * @var float $reductionDebtScore Trend score on company reduction in debt indicator
     */
    private $reductionDebtScore;

    /**
     * @return string
     */
    public function getRisk()
    {
        return $this->risk;
    }

    /**
     * @param string $risk
     * @return CashFlow
     */
    public function setRisk($risk)
    {
        $this->risk = $risk;
        return $this;
    }

    /**
     * @return float
     */
    public function getCashFlowScore()
    {
        return $this->cashFlowScore;
    }

    /**
     * @param float $cashFlowScore
     * @return CashFlow
     */
    public function setCashFlowScore($cashFlowScore)
    {
        $this->cashFlowScore = $cashFlowScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getCashSalesScore()
    {
        return $this->cashSalesScore;
    }

    /**
     * @param float $cashSalesScore
     * @return CashFlow
     */
    public function setCashSalesScore($cashSalesScore)
    {
        $this->cashSalesScore = $cashSalesScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getCashProductionScore()
    {
        return $this->cashProductionScore;
    }

    /**
     * @param float $cashProductionScore
     * @return CashFlow
     */
    public function setCashProductionScore($cashProductionScore)
    {
        $this->cashProductionScore = $cashProductionScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getCashOperationsScore()
    {
        return $this->cashOperationsScore;
    }

    /**
     * @param float $cashOperationsScore
     * @return CashFlow
     */
    public function setCashOperationsScore($cashOperationsScore)
    {
        $this->cashOperationsScore = $cashOperationsScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getOperatingCashGenerationScore()
    {
        return $this->operatingCashGenerationScore;
    }

    /**
     * @param float $operatingCashGenerationScore
     * @return CashFlow
     */
    public function setOperatingCashGenerationScore($operatingCashGenerationScore)
    {
        $this->operatingCashGenerationScore = $operatingCashGenerationScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getFreeCashFlowScore()
    {
        return $this->freeCashFlowScore;
    }

    /**
     * @param float $freeCashFlowScore
     * @return CashFlow
     */
    public function setFreeCashFlowScore($freeCashFlowScore)
    {
        $this->freeCashFlowScore = $freeCashFlowScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getCashFlowAfterFinExScore()
    {
        return $this->cashFlowAfterFinExScore;
    }

    /**
     * @param float $cashFlowAfterFinExScore
     * @return CashFlow
     */
    public function setCashFlowAfterFinExScore($cashFlowAfterFinExScore)
    {
        $this->cashFlowAfterFinExScore = $cashFlowAfterFinExScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getCashFlowAfterFinResultScore()
    {
        return $this->cashFlowAfterFinResultScore;
    }

    /**
     * @param float $cashFlowAfterFinResultScore
     * @return CashFlow
     */
    public function setCashFlowAfterFinResultScore($cashFlowAfterFinResultScore)
    {
        $this->cashFlowAfterFinResultScore = $cashFlowAfterFinResultScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getReductionDebtScore()
    {
        return $this->reductionDebtScore;
    }

    /**
     * @param float $reductionDebtScore
     * @return CashFlow
     */
    public function setReductionDebtScore($reductionDebtScore)
    {
        $this->reductionDebtScore = $reductionDebtScore;
        return $this;
    }

}