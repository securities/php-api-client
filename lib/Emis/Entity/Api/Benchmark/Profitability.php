<?php

namespace Emis\Entity\Api\Benchmark;


class Profitability
{
    /**
     * @var float $netIncomeNetSalesScore Benchmark score on company net income vs net sales indicator
     */
    private $netIncomeNetSalesScore;

    /**
     * @var int $netIncomeNetSalesRanking Benchmark ranking on company net income vs net sales indicator
     */
    private $netIncomeNetSalesRanking;

    /**
     * @var float $returnOnAssetsScore Benchmark score on company return on assets indicator
     */
    private $returnOnAssetsScore;

    /**
     * @var int $returnOnAssetsRanking Benchmark ranking on company return on assets indicator
     */
    private $returnOnAssetsRanking;

    /**
     * @var float $netIncomeNetWorthScore Benchmark score on company net income vs net worth indicator
     */
    private $netIncomeNetWorthScore;

    /**
     * @var int $netIncomeNetWorthRanking Benchmark ranking on company net income vs net worth indicator
     */
    private $netIncomeNetWorthRanking;

    /**
     * @var float $averageProfitabilityScore Average benchmark score on company profitability-related indicators
     */
    private $averageProfitabilityScore;

    /**
     * @var int $averageProfitabilityRanking Average benchmark ranking on company profitability-related indicators
     */
    private $averageProfitabilityRanking;

    /**
     * @return float
     */
    public function getNetIncomeNetSalesScore()
    {
        return $this->netIncomeNetSalesScore;
    }

    /**
     * @param float $netIncomeNetSalesScore
     * @return Profitability
     */
    public function setNetIncomeNetSalesScore($netIncomeNetSalesScore)
    {
        $this->netIncomeNetSalesScore = $netIncomeNetSalesScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getNetIncomeNetSalesRanking()
    {
        return $this->netIncomeNetSalesRanking;
    }

    /**
     * @param int $netIncomeNetSalesRanking
     * @return Profitability
     */
    public function setNetIncomeNetSalesRanking($netIncomeNetSalesRanking)
    {
        $this->netIncomeNetSalesRanking = $netIncomeNetSalesRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getReturnOnAssetsScore()
    {
        return $this->returnOnAssetsScore;
    }

    /**
     * @param float $returnOnAssetsScore
     * @return Profitability
     */
    public function setReturnOnAssetsScore($returnOnAssetsScore)
    {
        $this->returnOnAssetsScore = $returnOnAssetsScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getReturnOnAssetsRanking()
    {
        return $this->returnOnAssetsRanking;
    }

    /**
     * @param int $returnOnAssetsRanking
     * @return Profitability
     */
    public function setReturnOnAssetsRanking($returnOnAssetsRanking)
    {
        $this->returnOnAssetsRanking = $returnOnAssetsRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getNetIncomeNetWorthScore()
    {
        return $this->netIncomeNetWorthScore;
    }

    /**
     * @param float $netIncomeNetWorthScore
     * @return Profitability
     */
    public function setNetIncomeNetWorthScore($netIncomeNetWorthScore)
    {
        $this->netIncomeNetWorthScore = $netIncomeNetWorthScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getNetIncomeNetWorthRanking()
    {
        return $this->netIncomeNetWorthRanking;
    }

    /**
     * @param int $netIncomeNetWorthRanking
     * @return Profitability
     */
    public function setNetIncomeNetWorthRanking($netIncomeNetWorthRanking)
    {
        $this->netIncomeNetWorthRanking = $netIncomeNetWorthRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getAverageProfitabilityScore()
    {
        return $this->averageProfitabilityScore;
    }

    /**
     * @param float $averageProfitabilityScore
     * @return Profitability
     */
    public function setAverageProfitabilityScore($averageProfitabilityScore)
    {
        $this->averageProfitabilityScore = $averageProfitabilityScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getAverageProfitabilityRanking()
    {
        return $this->averageProfitabilityRanking;
    }

    /**
     * @param int $averageProfitabilityRanking
     * @return Profitability
     */
    public function setAverageProfitabilityRanking($averageProfitabilityRanking)
    {
        $this->averageProfitabilityRanking = $averageProfitabilityRanking;
        return $this;
    }

}