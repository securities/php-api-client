<?php

namespace Emis\Entity\Api\Benchmark;


class Indebtedness
{

    /**
     * @var float $indebtednessScore Benchmark score on company indebtedness indicator
     */
    private $indebtednessScore;

    /**
     * @var int $indebtednessRanking Benchmark ranking on company indebtedness indicator
     */
    private $indebtednessRanking;

    /**
     * @var float $leverageScore Benchmark score on company leverage indicator
     */
    private $leverageScore;

    /**
     * @var int $leverageRanking Benchmark ranking on company leverage indicator
     */
    private $leverageRanking;

    /**
     * @var float $totalLiabilitiesSalesScore Benchmark score on company total liabilities vs sales indicator
     */
    private $totalLiabilitiesSalesScore;

    /**
     * @var int $totalLiabilitiesSalesRanking Benchmark ranking on company total liabilities vs sales indicator
     */
    private $totalLiabilitiesSalesRanking;

    /**
     * @var float $averageIndebtednessScore Average benchmark score on company indebtedness-related indicators
     */
    private $averageIndebtednessScore;

    /**
     * @var int $averageIndebtednessRanking Average benchmark ranking on company indebtedness-related indicators
     */
    private $averageIndebtednessRanking;

    /**
     * @return float
     */
    public function getIndebtednessScore()
    {
        return $this->indebtednessScore;
    }

    /**
     * @param float $indebtednessScore
     * @return Indebtedness
     */
    public function setIndebtednessScore($indebtednessScore)
    {
        $this->indebtednessScore = $indebtednessScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getIndebtednessRanking()
    {
        return $this->indebtednessRanking;
    }

    /**
     * @param int $indebtednessRanking
     * @return Indebtedness
     */
    public function setIndebtednessRanking($indebtednessRanking)
    {
        $this->indebtednessRanking = $indebtednessRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getLeverageScore()
    {
        return $this->leverageScore;
    }

    /**
     * @param float $leverageScore
     * @return Indebtedness
     */
    public function setLeverageScore($leverageScore)
    {
        $this->leverageScore = $leverageScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getLeverageRanking()
    {
        return $this->leverageRanking;
    }

    /**
     * @param int $leverageRanking
     * @return Indebtedness
     */
    public function setLeverageRanking($leverageRanking)
    {
        $this->leverageRanking = $leverageRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalLiabilitiesSalesScore()
    {
        return $this->totalLiabilitiesSalesScore;
    }

    /**
     * @param float $totalLiabilitiesSalesScore
     * @return Indebtedness
     */
    public function setTotalLiabilitiesSalesScore($totalLiabilitiesSalesScore)
    {
        $this->totalLiabilitiesSalesScore = $totalLiabilitiesSalesScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalLiabilitiesSalesRanking()
    {
        return $this->totalLiabilitiesSalesRanking;
    }

    /**
     * @param int $totalLiabilitiesSalesRanking
     * @return Indebtedness
     */
    public function setTotalLiabilitiesSalesRanking($totalLiabilitiesSalesRanking)
    {
        $this->totalLiabilitiesSalesRanking = $totalLiabilitiesSalesRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getAverageIndebtednessScore()
    {
        return $this->averageIndebtednessScore;
    }

    /**
     * @param float $averageIndebtednessScore
     * @return Indebtedness
     */
    public function setAverageIndebtednessScore($averageIndebtednessScore)
    {
        $this->averageIndebtednessScore = $averageIndebtednessScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getAverageIndebtednessRanking()
    {
        return $this->averageIndebtednessRanking;
    }

    /**
     * @param int $averageIndebtednessRanking
     * @return Indebtedness
     */
    public function setAverageIndebtednessRanking($averageIndebtednessRanking)
    {
        $this->averageIndebtednessRanking = $averageIndebtednessRanking;
        return $this;
    }

}