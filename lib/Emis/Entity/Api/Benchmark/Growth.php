<?php
/**
 * Created by PhpStorm.
 * User: dbenov
 * Date: 8/28/2019
 * Time: 3:54 PM
 */

namespace Emis\Entity\Api\Benchmark;


class Growth
{
    /**
     * @var float $netSalesGrowthRateScore Benchmark score on company net sales growth rate indicator
     */
    private $netSalesGrowthRateScore;

    /**
     * @var int $netSalesGrowthRateRanking Benchmark ranking on company net sales growth rate indicator
     */
    private $netSalesGrowthRateRanking;

    /**
     * @var float $increaseDecreaseInAssetsScore Benchmark score on company increase vs decrease in assets indicator
     */
    private $increaseDecreaseInAssetsScore;

    /**
     * @var int $increaseDecreaseInAssetsRanking Benchmark ranking on company increase vs decrease in assets indicator
     */
    private $increaseDecreaseInAssetsRanking;

    /**
     * @var float $increaseDecreaseInNetIncomeScore Benchmark score on company increase vs decrease in net income indicator
     */
    private $increaseDecreaseInNetIncomeScore;

    /**
     * @var int $increaseDecreaseInNetIncomeRanking Benchmark ranking on company increase vs decrease in net income indicator
     */
    private $increaseDecreaseInNetIncomeRanking;

    /**
     * @var float $increaseDecreaseInEquityScore Benchmark score on company increase or decrease in equity indicator
     */
    private $increaseDecreaseInEquityScore;

    /**
     * @var int $increaseDecreaseInEquityRanking Benchmark ranking on company increase or decrease in equity indicator
     */
    private $increaseDecreaseInEquityRanking;

    /**
     * @var float $averageGrowthScore Average benchmark score on company growth-related indicators
     */
    private $averageGrowthScore;

    /**
     * @var int $averageGrowthRanking Average benchmark ranking on company growth-related indicators
     */
    private $averageGrowthRanking;

    /**
     * @return float
     */
    public function getNetSalesGrowthRateScore()
    {
        return $this->netSalesGrowthRateScore;
    }

    /**
     * @param float $netSalesGrowthRateScore
     * @return Growth
     */
    public function setNetSalesGrowthRateScore($netSalesGrowthRateScore)
    {
        $this->netSalesGrowthRateScore = $netSalesGrowthRateScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getNetSalesGrowthRateRanking()
    {
        return $this->netSalesGrowthRateRanking;
    }

    /**
     * @param int $netSalesGrowthRateRanking
     * @return Growth
     */
    public function setNetSalesGrowthRateRanking($netSalesGrowthRateRanking)
    {
        $this->netSalesGrowthRateRanking = $netSalesGrowthRateRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getIncreaseDecreaseInAssetsScore()
    {
        return $this->increaseDecreaseInAssetsScore;
    }

    /**
     * @param float $increaseDecreaseInAssetsScore
     * @return Growth
     */
    public function setIncreaseDecreaseInAssetsScore($increaseDecreaseInAssetsScore)
    {
        $this->increaseDecreaseInAssetsScore = $increaseDecreaseInAssetsScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getIncreaseDecreaseInAssetsRanking()
    {
        return $this->increaseDecreaseInAssetsRanking;
    }

    /**
     * @param int $increaseDecreaseInAssetsRanking
     * @return Growth
     */
    public function setIncreaseDecreaseInAssetsRanking($increaseDecreaseInAssetsRanking)
    {
        $this->increaseDecreaseInAssetsRanking = $increaseDecreaseInAssetsRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getIncreaseDecreaseInNetIncomeScore()
    {
        return $this->increaseDecreaseInNetIncomeScore;
    }

    /**
     * @param float $increaseDecreaseInNetIncomeScore
     * @return Growth
     */
    public function setIncreaseDecreaseInNetIncomeScore($increaseDecreaseInNetIncomeScore)
    {
        $this->increaseDecreaseInNetIncomeScore = $increaseDecreaseInNetIncomeScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getIncreaseDecreaseInNetIncomeRanking()
    {
        return $this->increaseDecreaseInNetIncomeRanking;
    }

    /**
     * @param int $increaseDecreaseInNetIncomeRanking
     * @return Growth
     */
    public function setIncreaseDecreaseInNetIncomeRanking($increaseDecreaseInNetIncomeRanking)
    {
        $this->increaseDecreaseInNetIncomeRanking = $increaseDecreaseInNetIncomeRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getIncreaseDecreaseInEquityScore()
    {
        return $this->increaseDecreaseInEquityScore;
    }

    /**
     * @param float $increaseDecreaseInEquityScore
     * @return Growth
     */
    public function setIncreaseDecreaseInEquityScore($increaseDecreaseInEquityScore)
    {
        $this->increaseDecreaseInEquityScore = $increaseDecreaseInEquityScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getIncreaseDecreaseInEquityRanking()
    {
        return $this->increaseDecreaseInEquityRanking;
    }

    /**
     * @param int $increaseDecreaseInEquityRanking
     * @return Growth
     */
    public function setIncreaseDecreaseInEquityRanking($increaseDecreaseInEquityRanking)
    {
        $this->increaseDecreaseInEquityRanking = $increaseDecreaseInEquityRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getAverageGrowthScore()
    {
        return $this->averageGrowthScore;
    }

    /**
     * @param float $averageGrowthScore
     * @return Growth
     */
    public function setAverageGrowthScore($averageGrowthScore)
    {
        $this->averageGrowthScore = $averageGrowthScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getAverageGrowthRanking()
    {
        return $this->averageGrowthRanking;
    }

    /**
     * @param int $averageGrowthRanking
     * @return Growth
     */
    public function setAverageGrowthRanking($averageGrowthRanking)
    {
        $this->averageGrowthRanking = $averageGrowthRanking;
        return $this;
    }

}