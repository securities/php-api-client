<?php
namespace Emis\Entity\Api\Benchmark;


class ProfitLoss
{
    /**
     * @var string $risk Risk scale of the company compared to the rest of the companies in the same industry based on the profit and loss score.
     */
    private $risk;

    /**
     * @var float $profitLossScore Total trend score on company profit and loss-related indicators
     */
    private $profitLossScore;

    /**
     * @var float $salesGrowthScore Trend score on company sales growth indicator
     */
    private $salesGrowthScore;

    /**
     * @var float $operatingProfitScore Trend score on company operating profit vs sales indicator
     */
    private $operatingProfitScore;

    /**
     * @var float $netProfitScore Trend score on company net profit vs sales indicator
     */
    private $netProfitScore;

    /**
     * @return string
     */
    public function getRisk()
    {
        return $this->risk;
    }

    /**
     * @param string $risk
     * @return ProfitLoss
     */
    public function setRisk($risk)
    {
        $this->risk = $risk;
        return $this;
    }

    /**
     * @return float
     */
    public function getProfitLossScore()
    {
        return $this->profitLossScore;
    }

    /**
     * @param float $profitLossScore
     * @return ProfitLoss
     */
    public function setProfitLossScore($profitLossScore)
    {
        $this->profitLossScore = $profitLossScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getSalesGrowthScore()
    {
        return $this->salesGrowthScore;
    }

    /**
     * @param float $salesGrowthScore
     * @return ProfitLoss
     */
    public function setSalesGrowthScore($salesGrowthScore)
    {
        $this->salesGrowthScore = $salesGrowthScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getOperatingProfitScore()
    {
        return $this->operatingProfitScore;
    }

    /**
     * @param float $operatingProfitScore
     * @return ProfitLoss
     */
    public function setOperatingProfitScore($operatingProfitScore)
    {
        $this->operatingProfitScore = $operatingProfitScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getNetProfitScore()
    {
        return $this->netProfitScore;
    }

    /**
     * @param float $netProfitScore
     * @return ProfitLoss
     */
    public function setNetProfitScore($netProfitScore)
    {
        $this->netProfitScore = $netProfitScore;
        return $this;
    }


}