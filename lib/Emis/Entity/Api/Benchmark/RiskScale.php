<?php
namespace Emis\Entity\Api\Benchmark;


class RiskScale
{
    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $description
     */
    private $description;

    public function __construct($code = null, $description = null){
        $this->setCode($code);
        $this->setDescription($description);
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Risk
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Risk
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


}