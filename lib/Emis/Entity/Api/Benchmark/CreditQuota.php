<?php

namespace Emis\Entity\Api\Benchmark;


class CreditQuota
{
    /**
     * @var string $currencyCode Currency code of the credit quota
     */
    private $currencyCode;

    /**
     * @var float $currencyMultiplier Currency multiplier
     */
    private $currencyMultiplier;

    /**
     * @var string $customerType Customer participation type
     */
    private $customerType;

    /**
     * @var float $participation Expected participation in customer's credit. It is based on the customer type.
     */
    private $participation;

    /**
     * @var float $creditQuotaValue Recommended credit quota value
     */
    private $creditQuotaValue;

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return CreditQuota
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * @return float
     */
    public function getCurrencyMultiplier()
    {
        return $this->currencyMultiplier;
    }

    /**
     * @param float $currencyMultiplier
     * @return CreditQuota
     */
    public function setCurrencyMultiplier($currencyMultiplier)
    {
        $this->currencyMultiplier = $currencyMultiplier;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * @param string $customerType
     * @return CreditQuota
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;
        return $this;
    }

    /**
     * @return float
     */
    public function getParticipation()
    {
        return $this->participation;
    }

    /**
     * @param float $participation
     * @return CreditQuota
     */
    public function setParticipation($participation)
    {
        $this->participation = $participation;
        return $this;
    }

    /**
     * @return float
     */
    public function getCreditQuotaValue()
    {
        return $this->creditQuotaValue;
    }

    /**
     * @param float $creditQuotaValue
     * @return CreditQuota
     */
    public function setCreditQuotaValue($creditQuotaValue)
    {
        $this->creditQuotaValue = $creditQuotaValue;
        return $this;
    }

}