<?php
namespace Emis\Entity\Api\Benchmark;

class TrendScore
{
    /**
     * @var int $code
     */
    private $code;

    /**
     * @var string $description
     */
    private $description;

    public function __construct($code = null, $description = null){
        $this->setCode($code);
        $this->setDescription($description);
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return TrendScore
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return TrendScore
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

}