<?php


namespace Emis\Entity\Api\Benchmark;


class Benchmark
{
    /**
     * @var int $isic Company id (EMISid)
     */
    private $isic;

    /**
     * @var \DateTime $generationDate The date on which the benchmark data is last updated
     */
    private $generationDate;

    /**
     * @var \Emis\Entity\Api\Benchmark\FinancialScore[] $financialScores Details about the calculated EMIS financial score
     */
    private $financialScores;

    /**
     * @return FinancialScore[]
     */
    public function getFinancialScores()
    {
        return $this->financialScores;
    }

    /**
     * @param FinancialScore[] $financialScores
     * @return Benchmark
     */
    public function setFinancialScores($financialScores)
    {
        $this->financialScores = $financialScores;
        return $this;
    }

    /**
     * @return Period
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param Period $period
     * @return Benchmark
     */
    public function setPeriod($period)
    {
        $this->period = $period;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsic()
    {
        return $this->isic;
    }

    /**
     * @param int $isic
     * @return Benchmark
     */
    public function setIsic($isic)
    {
        $this->isic = $isic;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getGenerationDate()
    {
        return $this->generationDate;
    }

    /**
     * @param \DateTime $generationDate
     * @return Benchmark
     */
    public function setGenerationDate($generationDate)
    {
        $this->generationDate = $generationDate;
        return $this;
    }
}