<?php
/**
 * Created by PhpStorm.
 * User: dbenov
 * Date: 8/28/2019
 * Time: 2:23 PM
 */

namespace Emis\Entity\Api\Benchmark;


class Trend
{
    /**
     * @var float $trendScore
     */
    private $trendScore;

    /**
     * @var string $risk
     */
    private $risk;

    /**
     * @var \Emis\Entity\Api\Benchmark\ProfitLoss $profitLoss
     */
    private $profitLoss;

    /**
     * @var \Emis\Entity\Api\Benchmark\Balance $balance
     */
    private $balance;

    /**
     * @var \Emis\Entity\Api\Benchmark\CashFlow $cashFlow
     */
    private $cashFlow;

    /**
     * @var float $financialScore
     */
    private $financialScore;

    /**
     * @return float
     */
    public function getTrendScore()
    {
        return $this->trendScore;
    }

    /**
     * @param float $trendScore
     * @return Trend
     */
    public function setTrendScore($trendScore)
    {
        $this->trendScore = $trendScore;
        return $this;
    }

    /**
     * @return string
     */
    public function getRisk()
    {
        return $this->risk;
    }

    /**
     * @param string $risk
     * @return Trend
     */
    public function setRisk($risk)
    {
        $this->risk = $risk;
        return $this;
    }

    /**
     * @return ProfitLoss
     */
    public function getProfitLoss()
    {
        return $this->profitLoss;
    }

    /**
     * @param ProfitLoss $profitLoss
     * @return Trend
     */
    public function setProfitLoss($profitLoss)
    {
        $this->profitLoss = $profitLoss;
        return $this;
    }

    /**
     * @return Balance
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param Balance $balance
     * @return Trend
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return CashFlow
     */
    public function getCashFlow()
    {
        return $this->cashFlow;
    }

    /**
     * @param CashFlow $cashFlow
     * @return Trend
     */
    public function setCashFlow($cashFlow)
    {
        $this->cashFlow = $cashFlow;
        return $this;
    }

    /**
     * @return float
     */
    public function getFinancialScore()
    {
        return $this->financialScore;
    }

    /**
     * @param float $financialScore
     * @return Trend
     */
    public function setFinancialScore($financialScore)
    {
        $this->financialScore = $financialScore;
        return $this;
    }

}