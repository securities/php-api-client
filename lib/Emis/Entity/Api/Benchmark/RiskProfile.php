<?php
namespace Emis\Entity\Api\Benchmark;


class RiskProfile
{
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $description
     */
    private $description;

    public function __construct($name = null, $description = null){
        $this->setName($name);
        $this->setDescription($description);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return RiskProfile
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return RiskProfile
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


}