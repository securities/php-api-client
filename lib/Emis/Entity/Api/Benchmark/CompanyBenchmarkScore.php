<?php

namespace Emis\Entity\Api\Benchmark;


class CompanyBenchmarkScore
{
    /**
     * @var float $benchmarkScore Company benchmark score. It shows the percentage of companies in the same industry, scored lower than the current one.
     */
    private $benchmarkScore;

    /**
     * @var float $averageRanking Average benchmark ranking of the company regarding all other companies in the same industry
     */
    private $averageRanking;

    /**
     * @var string $risk Risk scale of the company compared to the rest of the companies in the same industry based on the benchmark score.
     */
    private $risk;

    /**
     * @var \Emis\Entity\Api\Benchmark\Size $size Details about the company size-related indicators for benchmarking
     */
    private $size;

    /**
     * @var \Emis\Entity\Api\Benchmark\Growth $growth Details about the company growth-related indicators for benchmarking
     */
    private $growth;

    /**
     * @var \Emis\Entity\Api\Benchmark\Profitability $profitability Details about the company profitability-related indicators for benchmarking
     */
    private $profitability;

    /**
     * @var \Emis\Entity\Api\Benchmark\Indebtedness $indebtedness Details about the company indebtedness-related indicators for benchmarking
     */
    private $indebtedness;

    /**
     * @return float
     */
    public function getBenchmarkScore()
    {
        return $this->benchmarkScore;
    }

    /**
     * @param float $benchmarkScore
     * @return CompanyBenchmarkScore
     */
    public function setBenchmarkScore($benchmarkScore)
    {
        $this->benchmarkScore = $benchmarkScore;
        return $this;
    }

    /**
     * @return float
     */
    public function getAverageRanking()
    {
        return $this->averageRanking;
    }

    /**
     * @param float $averageRanking
     * @return CompanyBenchmarkScore
     */
    public function setAverageRanking($averageRanking)
    {
        $this->averageRanking = $averageRanking;
        return $this;
    }

    /**
     * @return string
     */
    public function getRisk()
    {
        return $this->risk;
    }

    /**
     * @param string $risk
     * @return CompanyBenchmarkScore
     */
    public function setRisk($risk)
    {
        $this->risk = $risk;
        return $this;
    }

    /**
     * @return Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param Size $size
     * @return CompanyBenchmarkScore
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return Growth
     */
    public function getGrowth()
    {
        return $this->growth;
    }

    /**
     * @param Growth $growth
     * @return CompanyBenchmarkScore
     */
    public function setGrowth($growth)
    {
        $this->growth = $growth;
        return $this;
    }

    /**
     * @return Profitability
     */
    public function getProfitability()
    {
        return $this->profitability;
    }

    /**
     * @param Profitability $profitability
     * @return CompanyBenchmarkScore
     */
    public function setProfitability($profitability)
    {
        $this->profitability = $profitability;
        return $this;
    }

    /**
     * @return Indebtedness
     */
    public function getIndebtedness()
    {
        return $this->indebtedness;
    }

    /**
     * @param Indebtedness $indebtedness
     * @return CompanyBenchmarkScore
     */
    public function setIndebtedness($indebtedness)
    {
        $this->indebtedness = $indebtedness;
        return $this;
    }

}