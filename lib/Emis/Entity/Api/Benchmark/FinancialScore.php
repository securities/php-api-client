<?php
namespace Emis\Entity\Api\Benchmark;

class FinancialScore
{
    /**
     * @var int $industryCode Industry code (NAICS)
     */
    private $industryCode;

    /**
     * @var string $scoreScale Final EMIS financial analysis score
     */
    private $financialRisk;

    /**
     * @var \Emis\Entity\Api\Benchmark\CompanyBenchmarkScore $benchmark Details about the calculated EMIS company benchmark score
     */
    private $benchmark;

    /**
     * @var \Emis\Entity\Api\Benchmark\Trend $trend Details about the calculated EMIS company trend score
     */
    private $trend;

    /**
     * @var \Emis\Entity\Api\Benchmark\CreditQuota $creditQuota Details about the calculated EMIS credit quota
     */
    private $creditQuota;

    /**
     * @var \Emis\Entity\Api\Benchmark\Period $period Details about the valid period, based on which EMIS financial and benchmark scores are calculated
     */
    private $period;

    /**
     * @return int
     */
    public function getIndustryCode()
    {
        return $this->industryCode;
    }

    /**
     * @param int $industryCode
     * @return FinancialScore
     */
    public function setIndustryCode($industryCode)
    {
        $this->industryCode = $industryCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFinancialRisk()
    {
        return $this->financialRisk;
    }

    /**
     * @param string $financialRisk
     * @return FinancialScore
     */
    public function setFinancialRisk($financialRisk)
    {
        $this->financialRisk = $financialRisk;
        return $this;
    }

    /**
     * @return CompanyBenchmarkScore
     */
    public function getBenchmark()
    {
        return $this->benchmark;
    }

    /**
     * @param CompanyBenchmarkScore $benchmark
     * @return FinancialScore
     */
    public function setBenchmark($benchmark)
    {
        $this->benchmark = $benchmark;
        return $this;
    }

    /**
     * @return Trend
     */
    public function getTrend()
    {
        return $this->trend;
    }

    /**
     * @param Trend $trend
     * @return FinancialScore
     */
    public function setTrend($trend)
    {
        $this->trend = $trend;
        return $this;
    }

    /**
     * @return CreditQuota
     */
    public function getCreditQuota()
    {
        return $this->creditQuota;
    }

    /**
     * @param CreditQuota $creditQuota
     * @return FinancialScore
     */
    public function setCreditQuota($creditQuota)
    {
        $this->creditQuota = $creditQuota;
        return $this;
    }

    /**
     * @return Period
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param Period $period
     * @return FinancialScore
     */
    public function setPeriod($period)
    {
        $this->period = $period;
        return $this;
    }

}