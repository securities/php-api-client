<?php
/**
 * Created by PhpStorm.
 * User: dbenov
 * Date: 8/28/2019
 * Time: 3:40 PM
 */

namespace Emis\Entity\Api\Benchmark;


class Size
{
    /**
     * @var float $salesScore Benchmark score on company sales indicator
     */
    private $salesScore;

    /**
     * @var int $salesRanking Benchmark ranking on company sales indicator
     */
    private $salesRanking;

    /**
     * @var float $assetsScore Benchmark score on company assets indicator
     */
    private $assetsScore;

    /**
     * @var int $assetsRanking Benchmark ranking on company assets indicator
     */
    private $assetsRanking;

    /**
     * @var float $profitScore Benchmark score on company profit indicator
     */
    private $profitScore;

    /**
     * @var int $profitRanking Benchmark ranking on company profit indicator
     */
    private $profitRanking;

    /**
     * @var float $totalShareholdersEquityScore Benchmark score on company total shareholders equity indicator
     */
    private $totalShareholdersEquityScore;

    /**
     * @var int $totalShareholdersEquityRanking Benchmark ranking on company total shareholders equity indicator
     */
    private $totalShareholdersEquityRanking;

    /**
     * @var float $averageSizeScore Average benchmark score on company size-related indicators
     */
    private $averageSizeScore;

    /**
     * @var int $averageSizeRanking Average benchmark ranking on company size-related indicators
     */
    private $averageSizeRanking;

    /**
     * @return float
     */
    public function getSalesScore()
    {
        return $this->salesScore;
    }

    /**
     * @param float $salesScore
     * @return Size
     */
    public function setSalesScore($salesScore)
    {
        $this->salesScore = $salesScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getSalesRanking()
    {
        return $this->salesRanking;
    }

    /**
     * @param int $salesRanking
     * @return Size
     */
    public function setSalesRanking($salesRanking)
    {
        $this->salesRanking = $salesRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getAssetsScore()
    {
        return $this->assetsScore;
    }

    /**
     * @param float $assetsScore
     * @return Size
     */
    public function setAssetsScore($assetsScore)
    {
        $this->assetsScore = $assetsScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getAssetsRanking()
    {
        return $this->assetsRanking;
    }

    /**
     * @param int $assetsRanking
     * @return Size
     */
    public function setAssetsRanking($assetsRanking)
    {
        $this->assetsRanking = $assetsRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getProfitScore()
    {
        return $this->profitScore;
    }

    /**
     * @param float $profitScore
     * @return Size
     */
    public function setProfitScore($profitScore)
    {
        $this->profitScore = $profitScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfitRanking()
    {
        return $this->profitRanking;
    }

    /**
     * @param int $profitRanking
     * @return Size
     */
    public function setProfitRanking($profitRanking)
    {
        $this->profitRanking = $profitRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalShareholdersEquityScore()
    {
        return $this->totalShareholdersEquityScore;
    }

    /**
     * @param float $totalShareholdersEquityScore
     * @return Size
     */
    public function setTotalShareholdersEquityScore($totalShareholdersEquityScore)
    {
        $this->totalShareholdersEquityScore = $totalShareholdersEquityScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalShareholdersEquityRanking()
    {
        return $this->totalShareholdersEquityRanking;
    }

    /**
     * @param int $totalShareholdersEquityRanking
     * @return Size
     */
    public function setTotalShareholdersEquityRanking($totalShareholdersEquityRanking)
    {
        $this->totalShareholdersEquityRanking = $totalShareholdersEquityRanking;
        return $this;
    }

    /**
     * @return float
     */
    public function getAverageSizeScore()
    {
        return $this->averageSizeScore;
    }

    /**
     * @param float $averageSizeScore
     * @return Size
     */
    public function setAverageSizeScore($averageSizeScore)
    {
        $this->averageSizeScore = $averageSizeScore;
        return $this;
    }

    /**
     * @return int
     */
    public function getAverageSizeRanking()
    {
        return $this->averageSizeRanking;
    }

    /**
     * @param int $averageSizeRanking
     * @return Size
     */
    public function setAverageSizeRanking($averageSizeRanking)
    {
        $this->averageSizeRanking = $averageSizeRanking;
        return $this;
    }

}