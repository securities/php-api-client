<?php
namespace Emis\Entity\Api\Dictionary;

class DataProvider
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return DataProvider
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return \Emis\Entity\Api\Dictionary\DataProvider
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}