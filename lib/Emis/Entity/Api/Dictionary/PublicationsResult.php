<?php
namespace Emis\Entity\Api\Dictionary;

class PublicationsResult
{
    /**
     * @var int
     */
    private $total;

    /**
     * @var \Emis\Entity\Api\Dictionary\Publication[]
     */
    private $publications = array();

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return PublicationsResult
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return Publication[]
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * @param Publication[] $publications
     * @return PublicationsResult
     */
    public function setPublications($publications)
    {
        $this->publications = $publications;
        return $this;
    }

    public function addPublication(Publication $publication)
    {
        $this->publications[] = $publication;
    }
}