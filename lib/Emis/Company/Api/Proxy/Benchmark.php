<?php


namespace Emis\Company\Api\Proxy;


class Benchmark extends Base
{
    /**
     * @param $isic
     * @return \Emis\Entity\Api\Benchmark\Benchmark
     * @throws \Exception
     */
    public function getCompanyBenchmark($isic) {
        $params = $this->getClient ()->paramsAsArray ( $this, 'getCompanyBenchmark', func_get_args () );

        /* @var \Emis\Entity\Api\Benchmark\Benchmark $result */
        $result = $this->getClient ()->request ( 'Benchmark', 'getCompanyBenchmark', $params );

        return $result;
    }

}